const json2csv = require('json2csv');

exports.createFile = (jsonData) =>
  json2csv({ data: jsonData, fields: ['name', 'orders'], fieldNames: ['Territory', 'NumberOfOrders'] });
