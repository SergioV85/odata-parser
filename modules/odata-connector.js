const request = require('request-promise');
const R = require('ramda');

const serverUrl = 'http://services.odata.org/V3/Northwind/Northwind.svc/';

const getEmployeesTerritories = () =>
  request(`${serverUrl}/Employees?$select=EmployeeID,Territories&$expand=Territories&$format=json`)
    .then((res) => JSON.parse(res))
    .then((res) => res.value);
const getOrders = () =>
  request(`${serverUrl}/Orders?$select=EmployeeID&$format=json`)
    .then((res) => JSON.parse(res))
    .then((res) => res.value);
const countOrders = R.pipe(
  R.groupBy(R.prop('EmployeeID')),
  R.map((R.length))
);
const flattenTerritories = R.pipe(
  R.map((territory) => R.pipe(
      R.prop('Territories'),
      R.map(R.merge({ EmployeeID: territory.EmployeeID }))
    )(territory)
  ),
  R.flatten,
  R.sortBy(R.prop('TerritoryID'))
);
const groupData = (orders, employees) => {
  const countedOrders = countOrders(orders);
  const groupedTerritories = flattenTerritories(employees);

  return R.map((territory) => ({
    id: territory.TerritoryID,
    name: territory.TerritoryDescription,
    orders: countedOrders[territory.EmployeeID]
  }), groupedTerritories);
};

exports.getAggregatedData = () => Promise.all([getOrders(), getEmployeesTerritories()])
  .then(([orders, employees]) => groupData(orders, employees));

