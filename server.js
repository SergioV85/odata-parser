const express = require('express');
const oDataSource = require('./modules/odata-connector');
const csvFileGenerator = require('./modules/csv-generator');

const app = express();

app.get('/', (req, res) => {
  oDataSource.getAggregatedData()
    .then((data) => {
      const csvFile = csvFileGenerator.createFile(data);
      res.attachment('result.csv');
      res.status(200).send(csvFile);
    });
});
app.listen(process.env.PORT || 4040);
